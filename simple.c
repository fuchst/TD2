
// --1
int maximum(int *t,int n){
    int *max = &t[0];
    for (int i = 0; i < n ; ++i) {
        if (t[i] > *max){
            max = &t[i];
        }
    }
    return *max;
}
void f(int a, int b, int *s, int *p) {
    *s = a + b;
    *p = a * b;
}
// --3
// la fonction va donc calculer la somme et le produit de a et b puis les stocker dans une variable à laquelle nous allons ajouter un pointeur.
//ce dernier sera ensuite utilis&e dans le main pour retrouver les résultat demandés 

// --4 / test
    //int tab[6] = {3,5,10,5,6,1};
    //int min,max;
    //minmax(tab,6,&min,&max);
void minmax(int *t, int n, int *pmin, int *pmax){
    pmin = &t[0];
    pmax = &t[0];
    for (int i = 0; i < n ; ++i) {
        if(t[i] < *pmin){
            pmin = &t[i];
        }
        if(t[i] > *pmax){
            pmax = &t[i];
        }
    }
    printf("valeur min: %i\n",*pmin);
    printf("la valeur pmax %i\n",*pmax);
}

// Création de tableaux dynamique

int copie(int *tab, int n) {
    int tab2[n];
    for (int i = 0; i < n; i++) {
        tab2[i] = tab[i];
        printf("%i ",tab2[i]);
    }
    return *tab2;
}
// --7 /test
//    int tab[13] = {0,2,3,4,6,8,9,3,5,6,7,2,3};
//    int * t = &tab;
//    copie(t,13);
int copie2(int *tab,int n){
    int *tab2;
    tab2 = malloc(sizeof (int) * n);
    for (int i = 0; i < n ; ++i) {
        tab2[i] = tab[i];
        printf("%i ", tab2[i]);
    }
    return *tab2;
}

// --8
int* unsurdeux(int *tab, int n){
    int * tab2;
    tab2 = malloc(sizeof (int) * n);
    int c =0;
    for (int i = 0; i < n ; i = i + 2) {
            tab2[c] = tab[i];
            printf("%i",tab2[c]);
            c++;
    }
    free(tab2);
    printf("\n");
    return tab2;
}

// Les matrices


int main(){
    int tab[13] = {0,2,3,4,6,8,9,3,5,6,7,2,3};
    int * t = &tab;
    unsurdeux(t,13);
}

